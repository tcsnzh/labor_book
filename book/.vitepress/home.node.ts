import matter from "gray-matter";
import fs from "fs/promises";

export type HomeAction<Id extends string | undefined> = {
  id: Id;
  theme: string;
  text: string;
  link: string;
};

export async function get_home_info(_obj: { docs_dir_suffixed: string }) {
  const { docs_dir_suffixed } = _obj;
  const _index_frontmatter = matter(
    await fs.readFile(`${docs_dir_suffixed}index.md`)
  );
  return {
    _index_frontmatter,
    home_title: _index_frontmatter.data.hero.name as string,
    home_description: _index_frontmatter.data.hero.description as string,
    home_actions: _index_frontmatter.data.hero.actions as Array<
      HomeAction<string | undefined>
    >,
  };
}

export function _require_home_action<Id extends string | undefined>(
  id: Id,
  home_actions: Awaited<ReturnType<typeof get_home_info>>["home_actions"]
): HomeAction<Id> {
  const existed = home_actions.find((it) => it.id === id);
  if (!existed) {
    throw new Error(`Not found home action matched , require id is ${id}`);
  }
  return existed as any;
}

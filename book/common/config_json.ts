export type BookConfig = {
  id: string;
  title: string;
  /**
   * 首页文件
   */
  index_fileid: string;
  /**
   * 创建者
   */
  creator: string;
  /**
   * 关键字
   */
  keywords: string[];
};

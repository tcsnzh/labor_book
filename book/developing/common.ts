import package_json from "../../package.json";
import { _replace_suffix } from "../../packages/utils/src/util";

export const gitlab_repository_url = _replace_suffix(
  package_json.repository.url,
  ".git",
  ""
);

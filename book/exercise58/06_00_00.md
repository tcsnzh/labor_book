---
internal58:
  authors:
    - 陈路冕
  reviewers:
    - 马逍
---

# 锻炼手册使用须知

<script lang="ts" setup>
import BookStatusBarExercise58 from "./component/BookStatusBarExercise58.vue";
</script>

<BookStatusBarExercise58 />

---

欢迎阅读劳动者锻炼手册 ，为了更好地帮助到你，请务必阅读本章节。

本书是为了向劳动者们普及锻炼、放松，以及运动康复方法，提高劳动者锻炼身体的自助互助能力而编写。

:::tip
详情请看“全部目录”。

- 如果是手机浏览器，“全部目录”按钮在左上方。

- 如果是 PDF 文件。目录在哪取决于你的 PDF 浏览器。
  :::

## 联系我们

联系方式 [参见 锻炼手册的交流与反馈](./06_03_00.md)

<!--@include: ../health58/contact_us.nodoc.md-->

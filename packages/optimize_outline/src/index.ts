import {
  ignore_suffix_zero,
  parse_prefix_number,
} from "../../utils/src/parse_prefix_number";
import { _has_property } from "../../utils/src/typed";
import lodash from "lodash";

export type OutlineTreeNode<
  ItemsFlag extends string,
  SUBTYPE extends OutlineTreeNode<ItemsFlag, SUBTYPE>
> = Record<ItemsFlag, SUBTYPE[]> | {};

export interface OptimizeOutlineContext<
  ItemsFlag extends string,
  N extends Readonly<OutlineTreeNode<ItemsFlag, N>>
> {
  readonly nodes: readonly N[];
  readonly items_flag: ItemsFlag;
  readonly hooks: {
    readonly allow_replace_parent___on_shorten_single_branch_treenode: (
      node: N,
      child_node: N
    ) => Promise<boolean>;
    // readonly allow_replace_parent___on_replace_when_the_child_node_prefix_number_are_samed: (
    //   node: N,
    //   child_node: N
    // ) => Promise<boolean>;
    // readonly insert_to_original_position_of_child___on_replace_when_the_child_node_prefix_number_are_samed: (
    //   node: N,
    //   child_node: N
    // ) => Promise<N[] | "concat">;
    readonly before_replace_to: (old: N, cur: N) => Promise<void>;
    readonly before_travel_node: (n: N, i: number) => Promise<void>;
    // readonly prefix_number: (n: N) => Promise<readonly number[]>;
  };
}

export async function optimize_outline<
  ItemsFlag extends string,
  N extends OutlineTreeNode<ItemsFlag, N>
>(_options: OptimizeOutlineContext<ItemsFlag, N>): Promise<N[]> {
  const { nodes, items_flag, hooks } = _options;

  let res_old = nodes;
  let res_new = nodes;
  do {
    res_old = res_new;
    res_new = await _shorten_single_branch_treenode({
      nodes: res_new,
      items_flag,
      hooks,
    });
    // res_new = await _replace_when_the_child_node_prefix_number_are_samed({
    //   nodes: res_new,
    //   items_flag,
    //   hooks,
    // });
  } while (!lodash.isEqual(res_old, res_new));
  return [...res_new];
}

async function _shorten_single_branch_treenode<
  ItemsFlag extends string,
  N extends OutlineTreeNode<ItemsFlag, N>
>(context: OptimizeOutlineContext<ItemsFlag, N>): Promise<N[]> {
  const { nodes, items_flag, hooks } = context;
  const result: N[] = [];
  for (let i = 0; i < nodes.length; i++) {
    const node = nodes[i];
    let node_target = node;

    while (
      _has_property<N, ItemsFlag, N[]>(node_target, items_flag) &&
      node_target[items_flag].length == 1 &&
      (await hooks.allow_replace_parent___on_shorten_single_branch_treenode(
        node_target,
        node_target[items_flag][0]
      ))
    ) {
      await hooks.before_travel_node(node_target, i);

      node_target = node_target[items_flag][0];
    }
    await hooks.before_travel_node(node_target, i);
    if (node !== node_target) {
      await hooks.before_replace_to(node, node_target);
    }
    result.push({
      ...node_target,
      [items_flag]: _has_property<N, ItemsFlag, N[]>(node_target, items_flag)
        ? await _shorten_single_branch_treenode({
            nodes: node_target[items_flag],
            items_flag,
            hooks,
          })
        : [],
    });
  }

  return result;
}

/**
 * Mapping the outline:
 *
 * ```
 * 1
 * |-1.0
 * |  |-1.0.0
 * |  |-1.0.1
 * |  |-1.0.2
 * |
 * |-1.1
 * |  |-1.1.0
 * ```
 *
 * To:
 *
 * ```
 * 1
 * |-1.0.0
 * |  |-1.0.1
 * |  |-1.0.2
 * |
 * |-1.1
 * ```
 *
 * @param context
 * @returns
 */
// async function _replace_when_the_child_node_prefix_number_are_samed<
//   ItemsFlag extends string,
//   N extends OutlineTreeNode<ItemsFlag, N>
// >(context: OptimizeOutlineContext<ItemsFlag, N>): Promise<N[]> {
//   const { nodes, items_flag, hooks } = context;
//   const result: N[] = [];
//   for (let i = 0; i < nodes.length; i++) {
//     const node = nodes[i];
//     await hooks.before_travel_node(node, i);

//     let node_target = node;
//     const nums_parent = ignore_suffix_zero(await hooks.prefix_number(node));

//     if (
//       _has_property<N, ItemsFlag, N[]>(node_target, items_flag) &&
//       node_target[items_flag].length > 0
//     ) {
//       for (let i = 0; i < node_target[items_flag].length; i++) {
//         const child = node_target[items_flag][i];
//         const nums_child = ignore_suffix_zero(await hooks.prefix_number(child));
//         if (
//           lodash.isEqual(nums_parent, nums_child) &&
//           (await hooks.allow_replace_parent___on_replace_when_the_child_node_prefix_number_are_samed(
//             node_target,
//             child
//           ))
//         ) {
//           const node_children_exclude_child_before = node_target[
//             items_flag
//           ].slice(0, i);
//           const node_children_exclude_child_after =
//             i + 1 < node_target[items_flag].length
//               ? node_target[items_flag].slice(i + 1)
//               : [];
//           const nodes_insert_to =
//             await hooks.insert_to_original_position_of_child___on_replace_when_the_child_node_prefix_number_are_samed(
//               node_target,
//               child
//             );
//           if (nodes_insert_to == "concat") {
//             node_target = {
//               ...child,
//               [items_flag]: [
//                 ...node_children_exclude_child_before,
//                 ...(_has_property<N, ItemsFlag, N[]>(child, items_flag)
//                   ? child[items_flag]
//                   : []),
//                 ...node_children_exclude_child_after,
//               ],
//             };
//           } else {
//             node_target = {
//               ...child,
//               [items_flag]: nodes_insert_to,
//             };
//           }
//           break;
//         }
//       }
//     }

//     if (node !== node_target) {
//       await hooks.before_replace_to(node, node_target);
//     }
//     result.push({
//       ...node_target,
//       [items_flag]: _has_property<N, ItemsFlag, N[]>(node_target, items_flag)
//         ? await _replace_when_the_child_node_prefix_number_are_samed({
//             nodes: node_target[items_flag],
//             items_flag,
//             hooks,
//           })
//         : [],
//     });
//   }

//   return result;
// }

export function filepath_to_prefix_number_info(filepath: string) {
  const filepath_replaced = filepath.replaceAll("\\", "/");
  const last_idx_of_split_char = filepath_replaced.lastIndexOf("/");
  const filename =
    last_idx_of_split_char < filepath.length - 1
      ? filepath_replaced.slice(last_idx_of_split_char + 1)
      : filepath_replaced;

  const res = parse_prefix_number(filename, ["_"], (it) => it);

  return {
    ...res,
    filename,
  };
}

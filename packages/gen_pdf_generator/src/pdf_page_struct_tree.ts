import {
  DocMetaPageStructTreeNode,
  OutlineNode,
  OutlineNodeLevel,
  OutlineNodePayload,
  TextContentItem,
} from "./indexing_pdf.node";

export type TitleInfo = {
  text: string;
};

export class PdfPageStructTreeReader<P extends OutlineNodePayload> {
  constructor() {}

  state: "can_read" | "written" = "can_read";
  titles: {
    level: number;
    info: TitleInfo;
    payload: P;
  }[] = [];

  public async reading(_obj: {
    root: DocMetaPageStructTreeNode<"Root">;
    text_content_items: TextContentItem[];
    payload: P;
  }): Promise<void> {
    if (this.state == "written") {
      throw new Error(
        "It is not recommended to continue read input after write output ."
      );
    }
    const { root, text_content_items, payload } = _obj;
    return await this._reading({
      node: root,
      text_content_items,
      payload,
    });
  }

  async _reading(_obj: {
    node: DocMetaPageStructTreeNode<string>;
    text_content_items: TextContentItem[];
    payload: P;
  }): Promise<void> {
    const { node, text_content_items, payload } = _obj;
    if (node.role == undefined) {
      return;
    }
    const level = [1, 2, 3, 4, 5, 6].find((it) => `H${it}` == node.role);
    if (level != undefined) {
      this.titles.push({
        level,
        info: await this._find_titled_node_info({
          node,
          text_content_items,
        }),
        payload,
      });
    }
    for (const child of node.children) {
      try {
        await this._reading({
          node: child,
          text_content_items,
          payload,
        });
      } catch (err) {
        throw new Error(
          `Error on reading child .
        child  node is ${JSON.stringify(child)},
        parent node is ${JSON.stringify(
          node,
          (k, v) => (k == "children" ? "(...)" : v),
          2
        )}
        `,
          { cause: err }
        );
      }
    }
  }

  async _find_titled_node_info(_obj: {
    node: DocMetaPageStructTreeNode<string> & { role: string };
    text_content_items: TextContentItem[];
  }): Promise<TitleInfo> {
    const { node, text_content_items } = _obj;
    if (node.children.length != 1 || node.children[0].role != "NonStruct") {
      throw new Error(
        `Unexpect title node format , there is not only one {role:"NonStruct"} child , but node is ${JSON.stringify(
          node,
          null,
          2
        )}`
      );
    }
    const finded_text_content_list = [];
    loop_every_str_node: for (const id_node of node.children[0].children) {
      try {
        if (id_node.role != undefined) {
          throw new Error(
            `Unexpect title node format , the NonStruct children should be {type,id} , but node is ${JSON.stringify(
              node,
              null,
              2
            )}`
          );
        }
        const text_content_start_index = text_content_items.findIndex(
          (it) => "id" in it && it.id == id_node.id
        );
        if (text_content_start_index < 0) {
          throw new Error(
            `Not found TextContent which id is ${id_node} , node is ${JSON.stringify(
              node,
              null,
              2
            )}`
          );
        }
        let indent_of_XXXMarkedContent = 0;
        // travel TextContent
        for (
          let i = text_content_start_index;
          i < text_content_items.length;
          i++
        ) {
          const text_content_item = text_content_items[i];
          if ("type" in text_content_item) {
            switch (text_content_item.type) {
              case "beginMarkedContentProps":
                indent_of_XXXMarkedContent++;
                break;
              case "endMarkedContent":
                indent_of_XXXMarkedContent--;
                break;
            }
          } else {
            finded_text_content_list.push(text_content_item);
          }
          if (indent_of_XXXMarkedContent < 0) {
            throw new Error("BUG");
          } else if (indent_of_XXXMarkedContent == 0) {
            continue loop_every_str_node;
          } else {
            continue;
          }
        }
        throw new Error(
          `Not found closed endMarkedContent in textContentItems ,
              indent_of_XXXMarkedContent is ${indent_of_XXXMarkedContent} ,
              text_content_start_index   is ${text_content_start_index} ,
          `
        );
      } catch (err) {
        throw new Error(
          `Error when find title text in node.children !
              
          finded_text_content_list  is ${JSON.stringify(
            finded_text_content_list
          )} ,
          text_content_items.length is ${text_content_items.length} ,
          id_node                   is ${JSON.stringify(id_node, null, 2)} ,
          node                      is ${JSON.stringify(node)} ,

        `,
          { cause: err }
        );
      }
    }

    const text = finded_text_content_list
      .map((it) => it.str.trim())
      .filter((it) => it != "")
      .join(" ");

    if (text == "") {
      throw new Error(`Unexpect empty text ?
      finded_text_content_list is ${JSON.stringify(
        finded_text_content_list,
        null,
        2
      )}
      `);
    }

    return {
      text,
    };
  }

  public async write_nodes<L extends OutlineNodeLevel>(_obj: {
    current_level: L;
  }): Promise<OutlineNode<L>[]> {
    this.state = "written";
    return (
      this.titles
        .map((t) => ({
          level: t.level as L,
          id: t.info.text,
          title: t.info.text,
          payload: t.payload,
          children: [],
        }))
        // array to tree
        .reduce<OutlineNode<L>[]>((acc, cur) => {
          let p = acc as OutlineNode<number>[]; // children array of last node's parent
          for (let i = 1; i <= cur.level - 1; i++) {
            p = p[p.length - 1].children;
          }
          p.push(cur);
          return acc;
        }, [])
    );
  }
}

import {
  OutlineNodeLevel,
  OutlineNode,
  DocMeta,
  DocMetaPdfPage,
  OutlineNodePayload,
} from "./indexing_pdf.node";
import { PdfPageStructTreeReader } from "./pdf_page_struct_tree";

export async function _create_indexes_from_pdf_file_docmeta<
  L extends OutlineNodeLevel
>(_obj: {
  current_level: L;
  docmeta: DocMeta;
  _pdf_filepath: string;
  prefix_number: number[];
  on_pdf_page: (pdf_page: DocMetaPdfPage) => void;
}): Promise<{
  nodes: OutlineNode<L>[];
}> {
  try {
    const {
      current_level,
      docmeta,
      on_pdf_page,
      _pdf_filepath,
      prefix_number,
    } = _obj;

    const parser = new PdfPageStructTreeReader<OutlineNodePayload>();
    for (let i = 0; i < docmeta.pdf.pages.length; i++) {
      const pdf_page = docmeta.pdf.pages[i];
      try {
        if (i + 1 != pdf_page.index) {
          throw new Error(
            `docmeta.pdf.page[${i}] number misalignment , actual ${
              i + 1
            } , expect ${pdf_page.index} ! It's ${JSON.stringify(
              pdf_page,
              null,
              2
            )}`
          );
        }
        on_pdf_page(pdf_page);
        const { page } = pdf_page;
        if (!page.structTree) {
          throw new Error(
            `Missing docmeta.pdf.page[${i}].structTree ! It's ${JSON.stringify(
              pdf_page,
              null,
              2
            )}`
          );
        }

        await parser.reading({
          root: page.structTree,
          text_content_items: page.textContent.items,
          payload: {
            t: "in_page",
            page_num: pdf_page.index,
            pdf_filepath: _pdf_filepath,
            prefix_number,
          },
        });
      } catch (err) {
        throw new Error(`Error on foreach docmeta.pdf.pages , i is ${i}`, {
          cause: err,
        });
      }
    }

    return {
      nodes: await parser.write_nodes({
        current_level,
      }),
    };
  } catch (err) {
    throw new Error(
      `Error on \`create_indexes_from_pdf_file_docmeta\` function . 
      _pdf_filepath  is ${_obj._pdf_filepath},
      current_level  is ${_obj.current_level}
    `,
      {
        cause: err,
      }
    );
  }
}

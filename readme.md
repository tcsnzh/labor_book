# Labor Book

## Description

## License 许可证 | 版权声明

> 本节所说的 **文档** 是指：
>
> - 本仓库、本仓库的构建产物（如GitlabPage页面） 中所有以 `.md`、`.pdf`、`.html` 为后缀名的文件 与 所有 *用于这些文本文档格式的文件* 的图片、视频文件。

此工程中的 **文档** 使用:

- [ ] TODO ...

- 若未声明原作者保留所有权利。

此工程的软件源代码（即本工程中所有不为 **文档** 的文件）使用 AGPL 3.0 许可证。

> GNU Affero 通用公共许可证的目的是保证您分享和改变一个程序的所有版本的自由--确保它对所有用户都是自由软件。
>
> 此许可证的英文原件位于: https://www.gnu.org/licenses/agpl-3.0.zh-cn.html
>
> 此许可证的中文译本位于: https://www.chinasona.org/gnu/agpl-3.0-cn.html
>
> [关于此许可证的法律效力 - 可参见 OSI 开源社区的这篇公众号文章: https://mp.weixin.qq.com/s/r1CHDYEZasGmGoL1IU0lRA

## 使用方式

在 [Gitlab Page](https://labor-book-tcsnzh-db51f1bb3e4c6d314b959b20a2f8ee686d4cb7316ea7c.gitlab.io/) 上查看本书:

## 贡献方式

### 贡献文档

如果你已经有使用 Git 的基础，请尽情的多多 Pull Request 。

如果你没有使用 Git 的经验，请参考：[如何编辑内容与提出意见](https://labor-book-tcsnzh-db51f1bb3e4c6d314b959b20a2f8ee686d4cb7316ea7c.gitlab.io/developing/01_00_00.html)

### 贡献代码

想开始贡献代码嘛？请参考 [readme_dev.md](./readme_dev.md) :

## Support

Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap

If you have ideas for releases in the future, it is a good idea to list them in the README.

## Authors and acknowledgment

Show your appreciation to those who have contributed to the project.

## 项目现状

我们的大典正在蒸蒸日上！

import { defineConfig } from "vitepress";
import { _common_sidebar } from "../../book/.vitepress/sidebar.node.ts";
import {
  get_home_info,
  _require_home_action,
} from "../../book/.vitepress/home.node.ts";

const { home_title, home_description, home_actions } = await get_home_info({
  docs_dir_suffixed: "./book_test_template/",
});
const health58_test_template1 = _require_home_action(
  "health58_test_template1",
  home_actions
);

//
// https://vitepress.dev/reference/site-config
//
// Run in Node.js env
//
export default defineConfig({
  lang: "zh-CN",
  title: home_title,
  description: home_description,
  vite: {
    configFile: "./vite.config.ts",
  },
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: "主页", link: "/" },
      {
        text: health58_test_template1.text,
        link: health58_test_template1.link,
      },
    ],

    lastUpdated: {
      text: "最后更新于 ",
      formatOptions: {
        dateStyle: "full",
        timeStyle: "medium",
      },
    },

    sidebar: [
      {
        text: health58_test_template1.text,
        items: await _common_sidebar({
          docs_dir_suffixed: "./book_test_template/",
          bookname: "health58_test_template1",
        }),
      },
    ],

    socialLinks: [],
  },
});
